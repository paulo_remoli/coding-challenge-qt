#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    // Converts the given string to a list of integers, it returns a boolean with the result of the operation.
    // listStr: String containing the input values
    // list: List of integers that will receive the result
    bool readList(const QString& listStr, QList<int>& list);

public slots:
    // Reads the input values and compare both lists, it shows the result in the result label.
    void compareClicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
