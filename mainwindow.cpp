#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->compareButton, SIGNAL(clicked()), this, SLOT(compareClicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::readList(const QString& listStr, QList<int>& list)
{
    QStringList valuesList = listStr.split(",", QString::SkipEmptyParts);

    foreach(QString num, valuesList)
    {
        bool ok = true;
        list.append(num.toInt(&ok));
        if(!ok)
        {
            return false;
        }
    }
    return true;
}



void MainWindow::compareClicked()
{
    QString list1Str = ui->list1Txt->text();
    QString list2Str = ui->list2Txt->text();

    QList<int> list1;
    QList<int> list2;

    // Validate each list
    if(!readList(list1Str, list1))
    {
        QMessageBox Msgbox;
        Msgbox.setText("The first list contains invalid values.");
        Msgbox.exec();
        ui->list1Txt->setFocus();
        return;
    }

    if(!readList(list2Str, list2))
    {
        QMessageBox Msgbox;
        Msgbox.setText("The second list contains invalid values.");
        Msgbox.exec();
        ui->list2Txt->setFocus();
        return;
    }

    // Compare lists and set show the result in the label
    if(list1 == list2)
    {
        ui->resultLabel->setText("True");
    }
    else
    {
        ui->resultLabel->setText("False");
    }

}
