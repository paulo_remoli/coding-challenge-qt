The task is to write a small Qt application with a minimal UI that allows to compare two lists of integers.
Requirements to the UI:

1. Add two textfields that allow to enter integer lists (separated by some separator)

2. Add a button that compares the two lists

3. Add a label that displays the result (e.g. true or false)

On button click a function for comparing the lists should be called.
The result of the comparison should then be displayed within the label.